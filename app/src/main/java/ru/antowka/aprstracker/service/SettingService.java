package ru.antowka.aprstracker.service;

public interface SettingService<T> {
    T loadSettings();
    void saveSettings(T settings);
}
