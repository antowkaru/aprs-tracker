package ru.antowka.aprstracker.service;

import ru.antowka.aprstracker.infrastructure.CustomLocationListener;
import ru.antowka.aprstracker.model.Location;

public interface GeoService {
    void setLocationListener(CustomLocationListener locationListener);
    void setLocation(android.location.Location androidLocation);
    void populateLocation(Location location);
    boolean hasNewLocation();
    CustomLocationListener getLocationListener();
}
