package ru.antowka.aprstracker.service;

import ru.antowka.aprstracker.model.APRSConfig;
import ru.antowka.aprstracker.model.Location;

public interface APRSService {
    String buildLocationString(APRSConfig config, Location location);
    void sendStringToAPRSServer(String string);
    String generateAPRSCode(String callsign);
}
