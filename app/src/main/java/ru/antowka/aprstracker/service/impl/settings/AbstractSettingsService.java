package ru.antowka.aprstracker.service.impl.settings;

import android.content.Context;
import android.content.SharedPreferences;
import android.widget.Toast;

import ru.antowka.aprstracker.helper.selializer.Serializer;
import ru.antowka.aprstracker.service.SettingService;

public abstract class AbstractSettingsService<T> implements SettingService<T> {


    protected SharedPreferences preferences;

    private Context context;

    protected Serializer<T> settingsSerializer;

    public AbstractSettingsService(Context context, String settingsName, Serializer<T> settingsSerializer) {
        this.context = context;
        this.settingsSerializer = settingsSerializer;
        preferences = context.getSharedPreferences(settingsName, Context.MODE_PRIVATE);
    }

    protected void saveSettings(String name, T settings) {

        final String json = settingsSerializer.serialize(settings);

        final SharedPreferences.Editor editor = preferences.edit();
        editor.putString(name, json);
        editor.apply();
        Toast.makeText(context, "Settings is saved", Toast.LENGTH_SHORT).show();
    }
}
