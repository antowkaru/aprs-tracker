package ru.antowka.aprstracker.service.impl;

import android.widget.Toast;

import com.bosphere.filelogger.FL;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import ru.antowka.aprstracker.R;
import ru.antowka.aprstracker.model.APRSConfig;
import ru.antowka.aprstracker.model.APRSIcon;
import ru.antowka.aprstracker.model.Location;
import ru.antowka.aprstracker.model.setting.impl.UserSettings;
import ru.antowka.aprstracker.service.APRSService;

public class APRSServiceImpl implements APRSService {

    private static final String HOST = "euro.aprs2.net";
    private static final Integer PORT = 14580;

    private static APRSService instance;

    private APRSConfig config;
    private Socket sock;
    private BufferedReader in;
    private PrintWriter out;

    public static APRSService getInstance(APRSConfig config) {

        if (instance == null) {
            instance = new APRSServiceImpl(config);
        }
        return instance;
    }

    public APRSServiceImpl(APRSConfig config) {
        if (config == null) return;
        this.config = config;
        connect();
    }

    @Override
    public String buildLocationString(APRSConfig config, Location location) {
        UserSettings userSettings = config.getUserSettings();
        return userSettings.getCallsign()
                + ">APRS:!"
                + formatLocation(location.getLatitude(), 8)
                + "/"
                + formatLocation(location.getLongitude(), 9)
                + APRSIcon.valueOf(userSettings.getIcon()).getIconSymbol()
                + ""
                + userSettings.getMessage()
                + ". App by UC6KFQ\n";
    }

    public String formatLocation(String latOrLong, int size) {

        int diff = latOrLong.length() - size;
        StringBuilder sb = new StringBuilder();
        for (; diff < 0; diff++) {
            sb.append("0");
        }
        return sb.append(latOrLong).toString();
    }

    @Override
    public void sendStringToAPRSServer(String string) {

        if (sock != null && sock.isConnected() && !sock.isClosed() && !sock.isOutputShutdown()) {

            if (out == null || in == null) {
                FL.i("Has problem with connection - start resend!!!");
                reSend(string);
            }

            //read response
            try {
                FL.i("Send APRS packet: " + string);

                out.println(string);
                out.flush();

                if (in != null && in.readLine() != null && !in.readLine().isEmpty()) {
                    FL.i("Response from APRS Server: " + in.readLine());
                    in = new BufferedReader(new InputStreamReader(sock.getInputStream()));
                } else {
                    reSend(string);
                }
            } catch (IOException e) {
                FL.i("Has problem with connection - start resend!!!");
                reSend(string);
            }
        } else {
            reSend(string);
        }
    }

    @Override
    public String generateAPRSCode(String callsign) {

        if (callsign == null || callsign.equals("")) {
            return "WRONG CALLSIGN";
        }

        callsign = callsign.toUpperCase().trim();
        Integer stophere = callsign.indexOf("-");

        if (stophere >= 0) {
            callsign = callsign.substring(stophere);
        }

        Integer hash = 0x73e2;
        int i = 0;
        int len = callsign.length();

        while (i < len) {
            hash ^= ord(callsign.substring(i, i+1)) << 8;
            if (len == i + 1) break;
            hash ^= ord(callsign.substring(i + 1, i + 2));
            i += 2;
        }

        return String.valueOf(hash & 0x7fff);
    }

    private int ord(String callsign) {
        final byte[] bytes = callsign.substring(0, 1).getBytes();
        final byte aByte = bytes[0];
        return aByte & 0xFF;
    }

    private void reSend(String string) {
        try {
            Thread.sleep(10000);                 //10 sec
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        FL.i("Starting reconnect and resend data to aprs server");
        connect();
        sendStringToAPRSServer(string);
    }

    private void connect() {
        try {
            sock = new Socket(HOST, PORT);
            in = new BufferedReader(new InputStreamReader(sock.getInputStream()));
            out = new PrintWriter(sock.getOutputStream());
            out.println(buildAuthenticationString(config));
            out.flush();
        } catch (IOException e) {
            FL.i("Could not open socket to aprs-server");
            try {
                Thread.sleep(10000);                 //10 sec
            } catch (InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
            connect();
        }
    }

    private String buildAuthenticationString(APRSConfig config) {
        return "user " + config.getUserSettings().getCallsign() + " pass " +
                config.getUserSettings().getPassword() + "\n";
    }
}
