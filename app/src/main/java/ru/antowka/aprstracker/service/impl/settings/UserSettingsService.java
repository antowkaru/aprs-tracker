package ru.antowka.aprstracker.service.impl.settings;

import android.content.Context;

import ru.antowka.aprstracker.factory.impl.UserPropertiesSerializerFactory;
import ru.antowka.aprstracker.model.setting.impl.UserSettings;

public class UserSettingsService extends AbstractSettingsService<UserSettings> {

    public UserSettingsService(Context context, UserPropertiesSerializerFactory factory) {
        super(context, "APPLICATION_" + UserSettings.USER_SETTINGS, factory.buildObject());
    }

    public UserSettings loadSettings() {
        return settingsSerializer.deserialize(preferences.getString(UserSettings.USER_SETTINGS, ""));
    }

    @Override
    public void saveSettings(UserSettings settings) {
        super.saveSettings(UserSettings.USER_SETTINGS, settings);
    }
}
