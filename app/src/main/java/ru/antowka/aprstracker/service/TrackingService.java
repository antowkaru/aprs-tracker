package ru.antowka.aprstracker.service;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.LocationManager;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;

import com.bosphere.filelogger.FL;

import java.util.Objects;

import ru.antowka.aprstracker.MainActivity;
import ru.antowka.aprstracker.R;
import ru.antowka.aprstracker.factory.impl.UserSettingsServiceFactory;
import ru.antowka.aprstracker.infrastructure.CustomLocationListener;
import ru.antowka.aprstracker.infrastructure.impl.BasicLocationListenerImpl;
import ru.antowka.aprstracker.infrastructure.impl.SmartLocationListenerImpl;
import ru.antowka.aprstracker.model.APRSConfig;
import ru.antowka.aprstracker.model.setting.impl.UserSettings;

public class TrackingService extends Service {

    private CustomLocationListener locationListener;
    private final APRSConfig config = new APRSConfig();
    public static final String START = "START";
    public static final String STOP = "STOP";

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        configInit();
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (config.getUserSettings().isAutoTrack()) {
            locationListener = new SmartLocationListenerImpl(locationManager, config, this);
        } else {
            locationListener = new BasicLocationListenerImpl(locationManager, config, this);
        }

        FL.i("Initialize GPS Listener");
    }

    @RequiresApi(api = Build.VERSION_CODES.ECLAIR)
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if (intent == null) {
            stopForeground(true);
            stopSelf();
            CustomLocationListener.isWorking = false;
            return super.onStartCommand(intent, flags, startId);
        }

        switch (Objects.requireNonNull(intent.getAction())) {

            case START:
                if (!CustomLocationListener.isWorking) {
                    startTracking(intent);
                    CustomLocationListener.isWorking = true;
                }
                break;

            case STOP:
                if (CustomLocationListener.isWorking) {
                    stopForeground(true);
                    stopSelf();
                    CustomLocationListener.isWorking = false;
                }
                break;
        }

        return super.onStartCommand(intent, flags, startId);
    }

    @TargetApi(Build.VERSION_CODES.O)
    @RequiresApi(api = Build.VERSION_CODES.ECLAIR)
    private void startTracking(Intent intent) {
        locationListener.startListener();

        String channelId = createNotificationChannel("aprs_tracker", "APRS Tracker");

        // Build the notification.
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, channelId);

        builder.setWhen(System.currentTimeMillis());
        builder.setSmallIcon(R.mipmap.ic_launcher);
        builder.setContentTitle(this.getResources().getString(R.string.app_name)).setOngoing(true);
        Bitmap largeIconBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher_foreground);
        builder.setLargeIcon(largeIconBitmap);
        // Make the notification max priority.
        builder.setPriority(Notification.PRIORITY_MAX);
        // Make head-up notification.
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);
        builder.setFullScreenIntent(pendingIntent, true);

        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, MainActivity.class), PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(contentIntent);

        // Add Pause button intent in notification.
        Intent pauseIntent = new Intent(this, TrackingService.class);
        pauseIntent.setAction(STOP);
        PendingIntent pendingPrevIntent = PendingIntent.getService(this, 0, pauseIntent, 0);
        NotificationCompat.Action prevAction = new NotificationCompat.Action(android.R.drawable.ic_lock_power_off, "Stop", pendingPrevIntent);
        builder.addAction(prevAction);

        // Start foreground service.
        startForeground(1, builder.build());
    }

    @RequiresApi(api = Build.VERSION_CODES.ECLAIR)
    @Override
    public void onDestroy() {
        stopTracking();
    }

    @RequiresApi(api = Build.VERSION_CODES.ECLAIR)
    private void stopTracking() {
        locationListener.stopListener();
        // Stop foreground service and remove the notification.
        stopForeground(true);
        // Stop the foreground service.
        stopSelf();
        CustomLocationListener.isWorking = false;
    }

    private void configInit() {
        SettingService<UserSettings> settingService = UserSettingsServiceFactory
                .getInstance()
                .setContext(getApplicationContext())
                .buildObject();

        config.setUserSettings(settingService.loadSettings());
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private String createNotificationChannel(String channelId, String channelName) {
        final NotificationChannel chan = new NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_NONE);
        chan.setLightColor(Color.BLUE);
        chan.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
        NotificationManager service = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        service.createNotificationChannel(chan);
        return channelId;
    }
}
