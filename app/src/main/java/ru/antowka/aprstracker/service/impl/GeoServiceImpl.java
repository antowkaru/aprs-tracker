package ru.antowka.aprstracker.service.impl;

import ru.antowka.aprstracker.infrastructure.CustomLocationListener;
import ru.antowka.aprstracker.model.Location;
import ru.antowka.aprstracker.service.GeoService;

public class GeoServiceImpl implements GeoService {

    private android.location.Location location;
    private boolean read;
    private CustomLocationListener locationListener;

    public void setLocationListener(CustomLocationListener locationListener) {
        this.locationListener = locationListener;
    }

    public CustomLocationListener getLocationListener() {
        return locationListener;
    }

    @Override
    public void setLocation(android.location.Location androidLocation) {
        this.location = androidLocation;
        read = true;
    }

    @Override
    public void populateLocation(Location location) {
        location.setLatitude(convertToAPRSFormatLocation(this.location.getLatitude(), 'N'));
        location.setLongitude(convertToAPRSFormatLocation(this.location.getLongitude(), 'E'));
        location.setLocation(this.location);
        read = false;
    }

    @Override
    public boolean hasNewLocation() {
        return read;
    }

    private String convertToAPRSFormatLocation(Double value, Character spell) {
        Integer degree = value.intValue();
        Double min = (value - degree) * 60;
        Integer minInt = min.intValue();
        Integer secInt = (int) ((min - minInt) * 100);
        return degree.toString() + (minInt < 10 ? "0" : "") + minInt + "." + (secInt < 10 ? "0" : "") + secInt + spell;
    }
}
