package ru.antowka.aprstracker.model;

import ru.antowka.aprstracker.model.setting.impl.UserSettings;
import ru.antowka.aprstracker.service.GeoService;

public class APRSConfig {

    private GeoService geoService;
    private UserSettings userSettings;

    public GeoService getGeoService() {
        return geoService;
    }

    public void setGeoService(GeoService geoService) {
        this.geoService = geoService;
    }

    public UserSettings getUserSettings() {
        return userSettings;
    }

    public void setUserSettings(UserSettings userSettings) {
        this.userSettings = userSettings;
    }
}
