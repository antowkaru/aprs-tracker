package ru.antowka.aprstracker.model;

public enum APRSIcon {
    PHONE("$"), JOGGER("["), CAR(">");

    private String icon;

    APRSIcon(String icon) {
        this.icon = icon;
    }

    public String getIconSymbol() {
        return icon;
    }

    public APRSIcon getByIconSymbol(String icon) {
        final APRSIcon[] values = APRSIcon.values();
        for (int i = 0; values.length > i; i++) {
            if (values[i].icon.equals(icon)) {
                return values[i];
            }
        }
        return null;
    }
}
