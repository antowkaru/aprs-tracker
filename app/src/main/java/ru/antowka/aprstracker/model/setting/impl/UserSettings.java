package ru.antowka.aprstracker.model.setting.impl;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ru.antowka.aprstracker.model.setting.Settings;

public class UserSettings implements Settings {

    public static final String USER_SETTINGS = "USER_SETTINGS";
    private String callsign;
    private String password;
    private String icon;
    private String message;
    private boolean isAutoTrack;
    private Integer distanceForUpdate;
    private Integer timeForUpdate;

    public String getCallsign() {
        return callsign;
    }

    public void setCallsign(String callsign) {
        this.callsign = callsign;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getDistanceForUpdate() {
        return distanceForUpdate;
    }

    public void setDistanceForUpdate(Integer distanceForUpdate) {
        this.distanceForUpdate = distanceForUpdate;
    }

    public Integer getTimeForUpdate() {
        return timeForUpdate;
    }

    public boolean isAutoTrack() {
        return isAutoTrack;
    }

    public void setAutoTrack(boolean autoTrack) {
        isAutoTrack = autoTrack;
    }

    public void setTimeForUpdate(Integer timeForUpdate) {
        this.timeForUpdate = timeForUpdate;
    }

    @JsonIgnore
    public boolean isEmptyRequiredFields() {
        return (callsign == null || callsign.isEmpty()) ||
                (password == null || password.isEmpty()) ||
                (icon == null || icon.isEmpty());
    }
}
