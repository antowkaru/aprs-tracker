package ru.antowka.aprstracker;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;

import ru.antowka.aprstracker.factory.impl.UserSettingsServiceFactory;
import ru.antowka.aprstracker.helper.ArrayUtils;
import ru.antowka.aprstracker.model.APRSIcon;
import ru.antowka.aprstracker.model.setting.impl.UserSettings;
import ru.antowka.aprstracker.service.impl.settings.UserSettingsService;

public class SettingsFragment extends Fragment {

    private Activity activity;

    private EditText callsignField;

    private EditText passwordField;

    private EditText messageField;

    private EditText timeForUpdate;

    private EditText distanceForUpdate;

    private Spinner iconSelector;

    private CheckBox isAutoTracking;

    private UserSettingsService userSettingsService;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View inflatedView = inflater.inflate(R.layout.setting_fragment, container, false);

        callsignField = inflatedView.findViewById(R.id.callsign);
        passwordField = inflatedView.findViewById(R.id.password);
        messageField = inflatedView.findViewById(R.id.message);
        distanceForUpdate = inflatedView.findViewById(R.id.distanceForUpdate);
        timeForUpdate = inflatedView.findViewById(R.id.timeForUpdate);
        iconSelector = inflatedView.findViewById(R.id.iconSelector);
        isAutoTracking = inflatedView.findViewById(R.id.autoTrackingMode);

        Button saveBtn = inflatedView.findViewById(R.id.saveBtn);

        iconSelector.setAdapter(
                new ArrayAdapter<>(getMainActivity(), android.R.layout.simple_spinner_dropdown_item, APRSIcon.values()));

        userSettingsService = (UserSettingsServiceFactory
                .getInstance()
                .setContext(getMainActivity()))
                .buildObject();

        final UserSettings userSettings = userSettingsService.loadSettings();

        callsignField.setText(userSettings.getCallsign());
        passwordField.setText(userSettings.getPassword());
        messageField.setText(userSettings.getMessage());
        if (userSettings.getIcon() != null) {
            iconSelector.setSelection(ArrayUtils.findIndex(APRSIcon.values(), APRSIcon.valueOf(userSettings.getIcon())));
        } else {
            iconSelector.setSelection(0);
        }
        if (userSettings.getTimeForUpdate() != null) {
            timeForUpdate.setText(userSettings.getTimeForUpdate().toString());
        }

        if (userSettings.getDistanceForUpdate() != null) {
            distanceForUpdate.setText(userSettings.getDistanceForUpdate().toString());
        }

        isAutoTracking.setChecked(userSettings.isAutoTrack());
        isAutoTracking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                distanceForUpdate.setEnabled(!isAutoTracking.isChecked());
                timeForUpdate.setEnabled(!isAutoTracking.isChecked());
            }
        });
        distanceForUpdate.setEnabled(!isAutoTracking.isChecked());
        timeForUpdate.setEnabled(!isAutoTracking.isChecked());

        saveBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                userSettings.setCallsign(callsignField.getText().toString());
                userSettings.setPassword(passwordField.getText().toString());
                userSettings.setMessage(messageField.getText().toString());
                userSettings.setIcon(iconSelector.getSelectedItem().toString());
                userSettings.setAutoTrack(isAutoTracking.isChecked());
                if (distanceForUpdate.getText() != null && !distanceForUpdate.getText().toString().isEmpty()) {
                    userSettings.setDistanceForUpdate(Integer.valueOf(distanceForUpdate.getText().toString()));
                }

                if (timeForUpdate.getText() != null && !timeForUpdate.getText().toString().isEmpty()) {
                    userSettings.setTimeForUpdate(Integer.valueOf(timeForUpdate.getText().toString()));
                }
                userSettingsService.saveSettings(userSettings);
            }
        });

        return inflatedView;
    }

    private Activity getMainActivity() {

        if (activity == null) {
            activity = getActivity();
        }

        if (activity == null) {
            throw new RuntimeException("Activity is empty");
        }

        return activity;
    }
}
