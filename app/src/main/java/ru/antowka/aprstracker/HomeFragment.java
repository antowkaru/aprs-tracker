package ru.antowka.aprstracker;


import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.bosphere.filelogger.FL;

import ru.antowka.aprstracker.factory.impl.InternetUtilsFactory;
import ru.antowka.aprstracker.factory.impl.UserSettingsServiceFactory;
import ru.antowka.aprstracker.helper.internet.InternetUtils;
import ru.antowka.aprstracker.infrastructure.CustomLocationListener;
import ru.antowka.aprstracker.model.setting.impl.UserSettings;
import ru.antowka.aprstracker.service.SettingService;
import ru.antowka.aprstracker.service.TrackingService;

public class HomeFragment extends Fragment {

    private Button btnStartTracking;

    private InternetUtils internetUtils = InternetUtilsFactory.getInstance().buildObject();


    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {


        final Activity activity = getActivity();
        View inflatedView = inflater.inflate(R.layout.home_fragment, container, false);

        if (activity == null) {
            System.out.println("context is empty");
            return null; //TODO: add handler
        }

        final Context context = container.getContext();
        checkPermissions(context, activity); //Проверка наличия разрешений

        btnStartTracking = inflatedView.findViewById(R.id.btnStartTracking);
        btnStartTracking.setText(CustomLocationListener.isWorking ? "Stop tracking" : "Start tracking");

        final LocationManager locationManager = (LocationManager) container
                .getContext().getSystemService(Context.LOCATION_SERVICE);

        final View.OnClickListener startTrackerListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                FL.i("OnClick, thread name: " + Thread.currentThread().getName());

                if (!CustomLocationListener.isWorking && !internetUtils.hasInetConnection()) {
                    Toast.makeText(activity, R.string.error_no_inet_connection_toastr, Toast.LENGTH_SHORT).show();
                    return;
                }

                if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    Toast
                            .makeText(container
                                    .getContext(), R.string.info_gps_was_disabled_toastr, Toast.LENGTH_SHORT)
                            .show();
                    return;
                }

                if (!CustomLocationListener.isWorking) {
                    startServiceWithGPSListener();
                    btnStartTracking.setText("Stop tracking");
                } else {
                    stopServiceWithGPSListener();
                    btnStartTracking.setText("Start tracking");
                }
            }
        };
        btnStartTracking.setOnClickListener(startTrackerListener);

        return inflatedView;
    }

    private void checkPermissions(Context context, Activity activity) {
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        }

        if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
        }

        if (ContextCompat.checkSelfPermission(context, Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.INTERNET}, 1);
        }

        if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_NETWORK_STATE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_NETWORK_STATE}, 1);
        }

        if (android.os.Build.VERSION.SDK_INT >= 28 && ContextCompat.checkSelfPermission(context, Manifest.permission.FOREGROUND_SERVICE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.FOREGROUND_SERVICE}, 1);
        }
    }

    private void stopServiceWithGPSListener() {
        final Activity activity = getActivity();
        if (activity != null) {
            final Intent trackingService = getService(activity);
            trackingService.setAction(TrackingService.STOP);
            activity.stopService(trackingService);
        }
    }

    private void startServiceWithGPSListener() {

        final Activity activity = getActivity();

        if (activity == null) {
            return;
        }

        SettingService<UserSettings> settingService = UserSettingsServiceFactory
                .getInstance()
                .setContext(getContext())
                .buildObject();

        final UserSettings userSettings = settingService.loadSettings();
        if (userSettings.isEmptyRequiredFields()) {
            Toast.makeText(activity, "Settings is wrong", Toast.LENGTH_LONG).show();
            return;
        }


        final Intent trackingService = getService(activity);
        trackingService.setAction(TrackingService.START);
        activity.startService(trackingService);
    }

    @NonNull
    private Intent getService(Activity activity) {
        return new Intent(activity, TrackingService.class);
    }
}
