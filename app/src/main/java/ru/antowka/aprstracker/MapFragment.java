package ru.antowka.aprstracker;

import android.annotation.SuppressLint;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import ru.antowka.aprstracker.factory.impl.UserSettingsServiceFactory;
import ru.antowka.aprstracker.model.setting.impl.UserSettings;
import ru.antowka.aprstracker.service.SettingService;

public class MapFragment extends Fragment {

    private UserSettings userSettings;
    private SettingService<UserSettings> settingService;

    @Override
    @SuppressLint("SetJavaScriptEnabled")
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View inflateView = inflater.inflate(R.layout.map_fragment, container, false);

        if (userSettings == null) {
            settingService = UserSettingsServiceFactory
                    .getInstance()
                    .setContext(getActivity()).buildObject();
            userSettings = settingService.loadSettings();
        }

        WebView mWebView = inflateView.findViewById(R.id.map_view);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.loadUrl("https://aprs.fi/#!call=a%2F" + userSettings.getCallsign() + "&timerange=3600&tail=3600");

        return inflateView;
    }
}
