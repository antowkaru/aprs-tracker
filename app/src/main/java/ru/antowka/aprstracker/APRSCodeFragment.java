package ru.antowka.aprstracker;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import ru.antowka.aprstracker.service.APRSService;
import ru.antowka.aprstracker.service.impl.APRSServiceImpl;

import static android.content.Context.CLIPBOARD_SERVICE;

public class APRSCodeFragment extends Fragment {

    private Activity activity;

    private EditText callsignField;

    private TextView aprsCodeField;

    private APRSService aprsService;

    private ClipboardManager clipboardManager;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View inflatedView = inflater.inflate(R.layout.aprs_code_fragment, container, false);
        clipboardManager = (ClipboardManager) getActivity().getSystemService(CLIPBOARD_SERVICE);
        aprsService = new APRSServiceImpl(null);

        callsignField = inflatedView.findViewById(R.id.callsign);
        aprsCodeField = inflatedView.findViewById(R.id.aprsCodeField);
        aprsCodeField.setEnabled(false);

        Button genBtn = inflatedView.findViewById(R.id.genBtn);
        genBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (callsignField.getText().length() > 0) {
                    final String code = aprsService.generateAPRSCode(callsignField.getText().toString());
                    aprsCodeField.setText(code);

                    ClipData clipData = ClipData.newPlainText("aprsCode", code);
                    clipboardManager.setPrimaryClip(clipData);
                    Toast.makeText(getContext(), "APRS Code copied to buffer", Toast.LENGTH_SHORT).show();
                }
            }
        });

        return inflatedView;
    }
}
