package ru.antowka.aprstracker;


import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;

import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.Toast;

import ru.antowka.aprstracker.factory.impl.InternetUtilsFactory;
import ru.antowka.aprstracker.helper.internet.InternetUtils;

public class MainActivity extends AppCompatActivity {

    private BottomNavigationView bottomNavigation;
    private FragmentManager fragmentManager;
    private HomeFragment homeFragment;
    private SettingsFragment settingsFragment;
    private MapFragment mapFragment;
    private APRSCodeFragment aprsCodeFragment;
    private Fragment fragment;
    private Activity activity;
    private InternetUtils internetUtils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.activity = this;
        bottomNavigation = findViewById(R.id.navigation_menu);
        fragmentManager = getSupportFragmentManager();

        internetUtils = InternetUtilsFactory
                .getInstance()
                .setContext(this)
                .buildObject();

        if (homeFragment == null) {
            homeFragment = new HomeFragment();
            fragment = homeFragment;
            setFragmentToActive();
        }

        bottomNavigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                switch (id) {

                    case R.id.navigation_home:
                        fragment = homeFragment;
                        break;

                    case R.id.navigation_map:

                        if (!internetUtils.hasInetConnection()) {
                            Toast
                                    .makeText(activity, R.string.error_no_inet_connection_toastr, Toast.LENGTH_SHORT)
                                    .show();
                            return true;
                        }

                        if (mapFragment == null) {
                            mapFragment = new MapFragment();
                        }
                        fragment = mapFragment;
                        break;

                    case R.id.navigation_settings:
                        if (settingsFragment == null) {
                            settingsFragment = new SettingsFragment();
                        }
                        fragment = settingsFragment;
                        break;

                    case R.id.navigation_aprs_code:
                        if (aprsCodeFragment == null) {
                            aprsCodeFragment = new APRSCodeFragment();
                        }
                        fragment = aprsCodeFragment;
                        break;
                }

                setFragmentToActive();
                return true;
            }
        });
    }

    private void setFragmentToActive() {
        final FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.main_container, fragment).commit();
    }
}
