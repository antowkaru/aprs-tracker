package ru.antowka.aprstracker.factory.impl;

import ru.antowka.aprstracker.service.impl.settings.UserSettingsService;

public class UserSettingsServiceFactory extends AbstractFactory<UserSettingsService> {

    private static UserSettingsServiceFactory instance;

    public static UserSettingsServiceFactory getInstance() {

        if (instance == null) {
            instance = new UserSettingsServiceFactory();
        }

        return instance;
    }

    @Override
    public UserSettingsService buildObject() {
        return new UserSettingsService(context, UserPropertiesSerializerFactory.getInstance());
    }
}
