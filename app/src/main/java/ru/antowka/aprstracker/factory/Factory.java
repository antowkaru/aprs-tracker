package ru.antowka.aprstracker.factory;

import android.content.Context;

public interface Factory<T> {
    T buildObject();

    Factory<T> setContext(Context context);
}
