package ru.antowka.aprstracker.factory.impl;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.ConnectivityManager;

import ru.antowka.aprstracker.helper.internet.InternetUtils;
import ru.antowka.aprstracker.helper.internet.impl.InternetUtilsImpl;

public class InternetUtilsFactory extends AbstractFactory<InternetUtils> {

    @SuppressLint("StaticFieldLeak")
    private static InternetUtilsFactory instance;

    private InternetUtils internetUtils;

    public static InternetUtilsFactory getInstance() {

        if (instance == null) {
            instance = new InternetUtilsFactory();
        }
        return instance;
    }

    @Override
    public InternetUtils buildObject() {

        if (internetUtils == null) {
            this.internetUtils = new InternetUtilsImpl(
                    (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        }

        return internetUtils;
    }
}
