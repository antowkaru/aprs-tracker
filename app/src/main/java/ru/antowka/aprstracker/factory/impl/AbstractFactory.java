package ru.antowka.aprstracker.factory.impl;

import android.content.Context;

import ru.antowka.aprstracker.factory.Factory;

public abstract class AbstractFactory<T> implements Factory<T> {

    protected Context context;

    @Override
    public AbstractFactory<T> setContext(Context context) {
        this.context = context;
        return this;
    }
}
