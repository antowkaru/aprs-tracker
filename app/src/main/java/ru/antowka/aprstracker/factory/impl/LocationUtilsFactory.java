package ru.antowka.aprstracker.factory.impl;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.LocationManager;

import ru.antowka.aprstracker.helper.location.impl.LocationUtilsImpl;
import ru.antowka.aprstracker.helper.location.LocationUtils;

public class LocationUtilsFactory extends AbstractFactory<LocationUtils> {

    @SuppressLint("StaticFieldLeak")
    private static LocationUtilsFactory instance;

    private LocationUtils locationUtils;

    public static LocationUtilsFactory getInstance() {

        if (instance == null) {
            instance = new LocationUtilsFactory();
        }
        return instance;
    }

    @Override
    public LocationUtils buildObject() {

        if (locationUtils == null) {
            final LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
            this.locationUtils = new LocationUtilsImpl(manager);
        }

        return locationUtils;
    }
}
