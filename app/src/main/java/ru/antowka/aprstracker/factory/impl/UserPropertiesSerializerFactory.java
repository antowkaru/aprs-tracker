package ru.antowka.aprstracker.factory.impl;

import ru.antowka.aprstracker.helper.selializer.impl.UserPropertiesSerializer;

public class UserPropertiesSerializerFactory extends AbstractFactory<UserPropertiesSerializer> {

    private static UserPropertiesSerializerFactory instance;

    public static UserPropertiesSerializerFactory getInstance() {

        if (instance == null) {
            instance = new UserPropertiesSerializerFactory();
        }

        return instance;
    }

    @Override
    public UserPropertiesSerializer buildObject() {
        return new UserPropertiesSerializer();
    }
}
