package ru.antowka.aprstracker.infrastructure.impl;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.widget.Toast;

import com.bosphere.filelogger.FL;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import ru.antowka.aprstracker.R;
import ru.antowka.aprstracker.factory.impl.LocationUtilsFactory;
import ru.antowka.aprstracker.helper.location.LocationUtils;
import ru.antowka.aprstracker.infrastructure.CustomLocationListener;
import ru.antowka.aprstracker.model.APRSConfig;
import ru.antowka.aprstracker.model.Location;
import ru.antowka.aprstracker.service.APRSService;
import ru.antowka.aprstracker.service.GeoService;
import ru.antowka.aprstracker.service.impl.APRSServiceImpl;
import ru.antowka.aprstracker.service.impl.GeoServiceImpl;

public class SmartLocationListenerImpl extends CustomLocationListener {

    public static final double FAST_SPEED = 100 / 3.6; // [m/s]
    public static final int FAST_RATE = 60;

    public static final double SLOW_SPEED = 5 / 3.6;  // [m/s]
    public static final int SLOW_RATE = 1200;

    public static final int TURN_TIME = 15;
    public static final int TURN_MIN = 10;
    public static final float TURN_SLOPE = 240;


    private static final int MIN_DISTANCE_CHANGE_FOR_UPDATES_METERS = 15;
    private static final float MIN_TIME_BW_UPDATES_MINUTES = 0.3f;

    private LocationManager locationManager;
    private APRSConfig config;
    private Context context;
    private final GeoService geoService = new GeoServiceImpl();
    private LocationUtils locationUtils;
    private android.location.Location prevLocation;
    private Criteria criteria;
    private ExecutorService es = Executors.newFixedThreadPool(2);
    private Float timeForUpdate;
    private Integer distanceForUpdate;

    public SmartLocationListenerImpl(LocationManager locationManager, APRSConfig config, Context context) {

        this.config = config;
        this.locationManager = locationManager;
        this.context = context;

        this.locationUtils = LocationUtilsFactory
                .getInstance()
                .setContext(context).buildObject();

        criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        criteria.setPowerRequirement(Criteria.POWER_LOW);
        criteria.setAltitudeRequired(false);
        criteria.setSpeedRequired(true);
        criteria.setCostAllowed(true);
        criteria.setBearingRequired(true);
        criteria.setHorizontalAccuracy(Criteria.ACCURACY_HIGH);
        criteria.setVerticalAccuracy(Criteria.NO_REQUIREMENT);

        timeForUpdate = MIN_TIME_BW_UPDATES_MINUTES;
        distanceForUpdate = MIN_DISTANCE_CHANGE_FOR_UPDATES_METERS;
    }

    @Override
    public void onLocationChanged(final android.location.Location location) {

        FL.i("OnChange, thread name: " + Thread.currentThread().getName());

        if (prevLocation != null) {
            final float distance = prevLocation.distanceTo(location);
            FL.i("GPS Accuracy: " + location.getAccuracy());

            if (distance < distanceForUpdate) {
                FL.i("Distance is small: " + distance + "m");
                return;
            }
        }

        prevLocation = location;
        geoService.setLocation(location);
        final Location myLocation = new Location();
        geoService.populateLocation(myLocation);

        es.execute(new Runnable() {
            @Override
            public void run() {
                sendObjectToAPRS(config, myLocation);
            }
        });
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {
        Toast
                .makeText(context, s, Toast.LENGTH_SHORT)
                .show();
    }

    @Override
    public void onProviderEnabled(String s) {
        Toast
                .makeText(context, R.string.info_gps_was_enabled_toastr, Toast.LENGTH_SHORT)
                .show();
    }

    @Override
    public void onProviderDisabled(String s) {
        Toast
                .makeText(context, R.string.info_gps_was_disabled_toastr, Toast.LENGTH_SHORT)
                .show();
    }

    public void startListener() {

        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        FL.i("Start GPS Listener");

        if (!locationUtils.isEnableGPS()) {
            Toast
                    .makeText(context, R.string.error_gps_is_disabled_toastr, Toast.LENGTH_SHORT)
                    .show();
            context.startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
            FL.i("GPS was disabled");
        }

        final int sec = (int) (timeForUpdate * 1000);
        locationManager
                .requestLocationUpdates(sec * 60, distanceForUpdate, criteria, this, null);
        CustomLocationListener.isWorking = true;
    }

    public void stopListener() {
        FL.i("Stop GPS Listener");
        locationManager.removeUpdates(this);
        CustomLocationListener.isWorking = false;
    }

    private void sendObjectToAPRS(APRSConfig config, Location location) {

        if (!smartBeaconCheck(location.getLocation())) {
            return;
        }

        APRSService aprsService = APRSServiceImpl.getInstance(config);
        final String locationString = aprsService.buildLocationString(config, location);
        aprsService.sendStringToAPRSServer(locationString);
    }

    private float getSpeed(android.location.Location location) {
        final float dist = location.distanceTo(prevLocation);
        final float diff = location.getTime() - prevLocation.getTime();
        return Math.max(Math.max(dist * 1000 / diff, location.getSpeed()), prevLocation.getSpeed());
    }

    private int getSpeedRate(float speed) {

        if (speed <= SLOW_SPEED) {
            return SLOW_RATE;
        }

        if (speed >= FAST_RATE) {
            return FAST_RATE;
        }

        return (int) (FAST_RATE + (SLOW_RATE - FAST_RATE) * (FAST_SPEED - speed) / (FAST_SPEED - SLOW_SPEED));
    }

    private float getBearingAngle(float alpha, float beta) {

        final float delta = Math.abs(alpha - beta) % 360;

        if (delta <= 180) {
            return delta;
        }

        return 360 - delta;
    }

    private boolean smartBeaconCornerPeg(android.location.Location location) {

        float speed = location.getSpeed();
        final float diff = location.getTime() - prevLocation.getTime();
        float turn = getBearingAngle(location.getBearing(), prevLocation.getBearing());

        if (!location.hasBearing() || speed == 0) {
            return false;
        }

        if (!prevLocation.hasBearing()) {
            return (diff / 1000 >= TURN_TIME);
        }

        double threshold = TURN_MIN + TURN_SLOPE / (speed * 2.23693629);

        return (diff / 1000 >= TURN_TIME && turn > threshold);
    }

    private boolean smartBeaconCheck(android.location.Location location) {
        if (prevLocation == null) {
            return true;
        }

        if (smartBeaconCornerPeg(location)) {
            return true;
        }

        final float dist = location.distanceTo(prevLocation);
        final float diff = location.getTime() - prevLocation.getTime();
        final float speed = getSpeed(location);
        final int speedRate = getSpeedRate(speed);

        return diff / 1000 >= speedRate;
    }
}
