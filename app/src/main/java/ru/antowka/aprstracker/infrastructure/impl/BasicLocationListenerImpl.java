package ru.antowka.aprstracker.infrastructure.impl;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.widget.Toast;

import com.bosphere.filelogger.FL;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import ru.antowka.aprstracker.R;
import ru.antowka.aprstracker.factory.impl.LocationUtilsFactory;
import ru.antowka.aprstracker.helper.location.LocationUtils;
import ru.antowka.aprstracker.infrastructure.CustomLocationListener;
import ru.antowka.aprstracker.model.APRSConfig;
import ru.antowka.aprstracker.model.Location;
import ru.antowka.aprstracker.service.APRSService;
import ru.antowka.aprstracker.service.GeoService;
import ru.antowka.aprstracker.service.impl.APRSServiceImpl;
import ru.antowka.aprstracker.service.impl.GeoServiceImpl;

public class BasicLocationListenerImpl extends CustomLocationListener {

    public static final int MIN_DISTANCE_CHANGE_FOR_UPDATES_METERS = 100;
    public static final int MIN_TIME_BW_UPDATES_MINUTES = 2;

    private LocationManager locationManager;
    private APRSConfig config;
    private Context context;
    private final GeoService geoService = new GeoServiceImpl();
    private LocationUtils locationUtils;
    private android.location.Location prevLocation;
    private Criteria criteria;
    private ExecutorService es = Executors.newFixedThreadPool(2);
    private Integer timeForUpdate;
    private Integer distanceForUpdate;

    public BasicLocationListenerImpl(LocationManager locationManager, APRSConfig config, Context context) {

        this.config = config;
        this.locationManager = locationManager;
        this.context = context;

        this.locationUtils = LocationUtilsFactory
                .getInstance()
                .setContext(context).buildObject();

        criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        criteria.setPowerRequirement(Criteria.POWER_LOW);
        criteria.setAltitudeRequired(false);
        criteria.setSpeedRequired(false);
        criteria.setCostAllowed(true);
        criteria.setBearingRequired(false);
        criteria.setHorizontalAccuracy(Criteria.ACCURACY_HIGH);
        criteria.setVerticalAccuracy(Criteria.NO_REQUIREMENT);

        timeForUpdate = config.getUserSettings().getTimeForUpdate() != null ? config.getUserSettings().getTimeForUpdate() : MIN_TIME_BW_UPDATES_MINUTES;
        distanceForUpdate = config.getUserSettings().getDistanceForUpdate() != null ? config.getUserSettings().getDistanceForUpdate() : MIN_DISTANCE_CHANGE_FOR_UPDATES_METERS;
    }

    @Override
    public void onLocationChanged(android.location.Location location) {

        FL.i("OnChange, thread name: " + Thread.currentThread().getName());

        if (prevLocation != null) {
            final float distance = prevLocation.distanceTo(location);
            FL.i("GPS Accuracy: " + location.getAccuracy());

            if (distance < distanceForUpdate) {
                FL.i("Distance is small: " + distance + "m");
                return;
            }
        }

        prevLocation = location;
        geoService.setLocation(location);
        final Location myLocation = new Location();
        geoService.populateLocation(myLocation);

        es.execute(new Runnable() {
            @Override
            public void run() {
                sendObjectToAPRS(config, myLocation);
            }
        });
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {
        Toast
                .makeText(context, s, Toast.LENGTH_SHORT)
                .show();
    }

    @Override
    public void onProviderEnabled(String s) {
        Toast
                .makeText(context, R.string.info_gps_was_enabled_toastr, Toast.LENGTH_SHORT)
                .show();
    }

    @Override
    public void onProviderDisabled(String s) {
        Toast
                .makeText(context, R.string.info_gps_was_disabled_toastr, Toast.LENGTH_SHORT)
                .show();
    }

    public void startListener() {

        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        FL.i("Start GPS Listener");

        if (!locationUtils.isEnableGPS()) {
            Toast
                    .makeText(context, R.string.error_gps_is_disabled_toastr, Toast.LENGTH_SHORT)
                    .show();
            context.startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
            FL.i("GPS was disabled");
        }

        final int sec = (int) (timeForUpdate * 1000);
        locationManager
                .requestLocationUpdates(sec * 60, distanceForUpdate, criteria, this, null);
        CustomLocationListener.isWorking = true;
    }

    public void stopListener() {
        FL.i("Stop GPS Listener");
        locationManager.removeUpdates(this);
        CustomLocationListener.isWorking = false;
    }

    private void sendObjectToAPRS(APRSConfig config, Location location) {

        APRSService aprsService = APRSServiceImpl.getInstance(config);
        final String locationString = aprsService.buildLocationString(config, location);
        aprsService.sendStringToAPRSServer(locationString);
    }
}
