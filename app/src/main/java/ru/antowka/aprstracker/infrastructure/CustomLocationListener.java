package ru.antowka.aprstracker.infrastructure;

import android.location.LocationListener;

public abstract class CustomLocationListener implements LocationListener {

    public static volatile boolean isWorking = false;

    public abstract void startListener();
    public abstract void stopListener();
}
