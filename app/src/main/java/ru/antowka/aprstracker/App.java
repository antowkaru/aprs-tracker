package ru.antowka.aprstracker;

import android.app.Application;
import android.content.Intent;
import android.os.Environment;

import com.bosphere.filelogger.FL;
import com.bosphere.filelogger.FLConfig;
import com.bosphere.filelogger.FLConst;

import java.io.File;

import ru.antowka.aprstracker.infrastructure.impl.BasicLocationListenerImpl;

public class App extends Application {

    @Override
    public void onCreate() {

        super.onCreate();

        FL.init(new FLConfig.Builder(this)
                .minLevel(FLConst.Level.V)   // customise minimum logging level
                .logToFile(true)   // enable logging to file
                .dir(new File(Environment.getDataDirectory(), "data/ru.antowka.aprstracker/log"))    // customise directory to hold log files
                .formatter(new FLConfig.DefaultFormatter())    // customise log format and file name
                .retentionPolicy(FLConst.RetentionPolicy.FILE_COUNT) // customise retention strategy
                .maxFileCount(FLConst.DEFAULT_MAX_FILE_COUNT)    // customise how many log files to keep if retention by file count
                .maxTotalSize(FLConst.DEFAULT_MAX_TOTAL_SIZE)    // customise how much space log files can occupy if retention by total size
                .build());

        FL.setEnabled(true);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        stopService(new Intent(getApplicationContext(), BasicLocationListenerImpl.class));
    }
}
