package ru.antowka.aprstracker.helper.internet.impl;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import ru.antowka.aprstracker.helper.internet.InternetUtils;

public class InternetUtilsImpl implements InternetUtils {

    private ConnectivityManager connectivityManager;

    public InternetUtilsImpl(ConnectivityManager connectivityManager) {
        this.connectivityManager = connectivityManager;
    }

    @Override
    public boolean hasInetConnection() {
        NetworkInfo netInfo = connectivityManager.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }
}
