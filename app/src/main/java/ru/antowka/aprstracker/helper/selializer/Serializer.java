package ru.antowka.aprstracker.helper.selializer;

public interface Serializer<T> {
    String serialize(T object);
    T deserialize(String json);
}
