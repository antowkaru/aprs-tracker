package ru.antowka.aprstracker.helper;

public class ArrayUtils {

    // Function to find the index of an element
    public static Integer findIndex(Object arr[], Object t) {

        if (t == null) return null;

        for (int i = 0; arr.length > i; i++) {
            if (t == arr[i] || t.equals(arr[i])) return i;
        }

        return null;
    }
}
