package ru.antowka.aprstracker.helper.selializer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public abstract class AbstractSerializer<T> implements Serializer<T> {

    protected ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public String serialize(T object) {

        try {
            return objectMapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return "{}";
    }

    @Override
    public abstract T deserialize(String json);
}
