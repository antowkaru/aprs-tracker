package ru.antowka.aprstracker.helper.internet;

public interface InternetUtils {
    boolean hasInetConnection();
}
