package ru.antowka.aprstracker.helper.selializer.impl;

import java.io.IOException;

import ru.antowka.aprstracker.helper.selializer.AbstractSerializer;
import ru.antowka.aprstracker.model.setting.impl.UserSettings;

public class UserPropertiesSerializer extends AbstractSerializer<UserSettings> {

    @Override
    public UserSettings deserialize(String json) {
        try {
            return objectMapper.readValue(json, UserSettings.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return new UserSettings();
    }
}
