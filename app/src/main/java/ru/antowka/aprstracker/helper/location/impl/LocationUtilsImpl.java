package ru.antowka.aprstracker.helper.location.impl;

import android.location.LocationManager;

import ru.antowka.aprstracker.helper.location.LocationUtils;

public class LocationUtilsImpl implements LocationUtils {

    private LocationManager locationManager;

    public LocationUtilsImpl(LocationManager locationManager) {
        this.locationManager = locationManager;
    }

    @Override
    public boolean isEnableGPS() {
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }
}
