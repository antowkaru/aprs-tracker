package ru.antowka.aprstracker;

import org.junit.Test;

import ru.antowka.aprstracker.service.APRSService;
import ru.antowka.aprstracker.service.impl.APRSServiceImpl;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 */
public class APRSServiceImplTest {
    @Test
    public void generateAPRSCode() {
        APRSService as = new APRSServiceImpl(null);
        final String aprsCode = as.generateAPRSCode("UC6KFQ");
        assertEquals("22203", aprsCode);
    }
}